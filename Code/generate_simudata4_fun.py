# Copyright (c) 2017 Zi Wang
from push_world import *
import numpy as np
import sys

# simulator for 4d pushing experiment
def push4d(rx, ry, init_angle, gx, gy, simu_steps):
    simu_steps = int(float(simu_steps) * 10)
    
    # set it to False if no gui needed
    world = b2WorldInterface(False)
    oshape, osize, ofriction, odensity, bfriction, hand_shape, hand_size  = 'circle', 1, 0.01, 0.05, 0.01, 'rectangle', (0.3,1) 
    thing,base = make_thing(500, 500, world, oshape, osize, ofriction, odensity, bfriction, (0,0))
    xvel = -rx;
    yvel = -ry;
    regu = np.linalg.norm([xvel,yvel])
    xvel = xvel / regu * 5;
    yvel = yvel / regu * 5;
    robot = end_effector(world, (rx,ry), base, init_angle, hand_shape, hand_size)
    ret = simu_push2(world, thing, robot, base, xvel, yvel, simu_steps)
    ret = np.linalg.norm(np.array([gx, gy]) - ret)

    return ret