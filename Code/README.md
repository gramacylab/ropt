# Robust expected improvement for Bayesian optimization code

All of this code was written by Ryan Christianson at rchristianson@vt.edu. This code is designed to produce the plots from the paper Robust expected improvement for Bayesian optimization. This work was coded in R version 4.0.2 and uses the following packages: laGP, ggplot2, gridExtra, lhs, foreach, doParallel, reticulate.

## Usage

The simple examples (Figures 1, 2, 3, 4) from the paper can be generated from Paper_Plots.R.

The file "Simple_Example.R" contains code to run through the entire example for a 1d toy function, figure 6 from the paper. It can be run by anyone to reproduce that Monte Carlo experiment just which will be identical to the paper modulo a random seed.

All of the other Results (Figures 5, 6, 7, 8, 9) from Section 4.2 can be reproduced by running the BO_Comparisons function to generate an RData file containing the Monte Carlo results and then plotting with Plot_BO_comparisons.R. BO_Comparisons.R relies on Optimizing_Nd.R, stableOPT.R and BO_Comparison_Funs.R to generate results.

Paper_Functions.R provides the basic plots of each of the functions we used which are shown alongside the results.

Example_Sample.R recreates Figure 10 which relies on Optimizing_Nd.R and stableOPT.R.

Finally, our robot pushing example from Section 5 can be found in Robot_Push.R. Running this file will create an RData file containing the results of the Monte Carlo experiment which can again be plotted by Plot_BO_Comparisons.R. This file relies on Optimizing_Nd.R, stabeleOPT.R and BO_Comparison_Funs.R to run our algorithms as well as Python code generate_simudata3_fun.py, generate_simudata4_fun.py and push_world.py (compiled to push_world.pyc) to run the Python simulator for the robot pushing experiment.

