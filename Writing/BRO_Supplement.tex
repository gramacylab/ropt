\documentclass[11pt]{article}
\usepackage{hyperref}
\usepackage{amsmath, amsfonts, amssymb, mathrsfs}
\usepackage{dcolumn}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{filemod}
\usepackage[ruled, vlined]{algorithm2e}
\usepackage{floatrow}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage[toc,page]{appendix}
\graphicspath{{./Figures/}}

\usepackage{natbib}

\addtolength{\oddsidemargin}{-.5in}%
\addtolength{\evensidemargin}{-.5in}%
\addtolength{\textwidth}{1in}%
\addtolength{\textheight}{1.3in}%
\addtolength{\topmargin}{-.8in}%
\makeatletter
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                     {-3.5ex \@plus -1ex \@minus -.2ex}%
                                     {2.3ex \@plus.2ex}%
                                     {\normalfont\fontfamily{phv}\fontsize{16}{19}\bfseries}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                       {-3.25ex\@plus -1ex \@minus -.2ex}%
                                       {1.5ex \@plus .2ex}%
                                       {\normalfont\fontfamily{phv}\fontsize{14}{17}\bfseries}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                       {-3.25ex\@plus -1ex \@minus -.2ex}%
                                       {1.5ex \@plus .2ex}%
                                       {\normalfont\normalsize\fontfamily{phv}\fontsize{14}{17}\selectfont}}
\makeatother

%\pdfminorversion=4
% NOTE: To produce blinded version, replace "0" with "1" below.
\newcommand{\blind}{0}

\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

%\newcommand{\blu}[1]{\textcolor{blue}{#1}} % blue for newest version
\newcommand{\blu}[1]{\textcolor{black}{#1}} % change edits back to black

\begin{document}
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\def\spacingset#1{\renewcommand{\baselinestretch}%
			{#1}\small\normalsize} \spacingset{1}
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		\if0\blind
		{
			\title{\bf Supplemental online materials for \emph{Robust expected 
			improvement for Bayesian optimization}}
			\author{Ryan B.~Christianson$^a$ and Robert B.~Gramacy$^b$ \\
			$^a$ Department of Statistics and Data Science, NORC at the 
			University of Chicago, Chicago, IL \\
             $^b$ Department of Statistics, Virginia Tech, Blacksburg, VA}
			\date{\today}
			\maketitle
		} \fi
		
		\if1\blind
		{

            \title{\bf Supplemental online materials for \emph{Robust expected 
            improvement for Bayesian optimization}}
			\author{Author information is purposely removed for double-blind 
			review}
			
\bigskip
			\bigskip
			\bigskip
			\begin{center}
				{\LARGE\bf \emph{IISE Transactions} \LaTeX \ Template}
			\end{center}
			\medskip
		} \fi
		\bigskip

\section{StableOPT}

\citeauthor{Bogunovic2018}~assume a fixed, known
$\alpha$, although we see no reason why our extensions for unknown $\alpha$
could not be adapted to their method as well. Their algorithm relies on
confidence bounds to narrow in on $x^r$.  Let $\mathrm{ucb}_n(x) = \mu_n(x) +
2\sigma_n(x)$ denote the upper 95\% confidence bound at $x$ for a fitted
surrogate $\hat{f}_n$, and similarly $\mathrm{lcb}_n(x) = \mu_n(x) -
2\sigma_n(x)$ for the analagous lower bound. Then we may translate their
algorithm into our notation, shown in Alg.~\ref{alg:stableopt}, furnishing the
$n^{\mathrm{th}}$ acquisition. Similar to REGO, this may then be wrapped in a
loop for multiple acquisitions. We could not find any public software for {\tt
StableOPT}, but it was relatively easy to implement in {\sf R}; see our public
Git repo.

\bigskip
\begin{algorithm}[H] 		
	\DontPrintSemicolon
	\textbf{input} $D_{n - 1} = (X_{n - 1}, Y_{n - 1})$ and $\alpha$\\
	$\hat{f}_{n - 1}(x) = \mathrm{GP}_{\hat{\theta}}(D_{n - 1})$
	$\tilde{x}_n = \argmin_{x \in [0, 1]^d} \max_{a \in [-\alpha, \alpha]} 
	\mathrm{lcb}_{n - 1}(x + a)$ \tcp*{where we think $x^r$ is}
	$a_n = \argmax_{a \in [-\alpha, \alpha]} \mathrm{ucb}_{n - 1}(\tilde{x}_n + 
	a)$ \tcp*{worst point within $\tilde{x}_n^\alpha$}
	\Return $(\tilde{x}_n + a_n, f(\tilde{x}_n + a_n)))$ \tcp*{sample from $f$ 
	and return}
	\caption{{\tt StableOPT}}
	\label{alg:stableopt}
\end{algorithm}
\noindent 
\bigskip

Rather than acquiring new runs nearby likely $x^r$, {\tt StableOPT} samples
the worst point within $\tilde{x}_n^\alpha$. Consequently, its final $X_N$
does not contain any points thought to be $x^r$. 
\citeauthor{Bogunovic2018}~recommend selecting $x^*_{\mathrm{bear}}$ from all 
$\tilde{x}_n$ rather than
the actual points sampled, all $\tilde{x}_n + a_n$, using notation introduced
in Alg.~\ref{alg:stableopt}. We generally think it is a mistake to report an
answer at an untried input location.  So in our experiments we
calculate $x^*_{\mathrm{bear}}$ for {\tt StableOPT} as derived from a final,
post hoc adversary calculation.

\section{Two dimensional example}

The top-left panel of Figure \ref{fig:bertsima2d} shows a test problem
from \cite{Bertsimas2010}, \blu{a common test case in robust 
optimization, which is} defined as:
\begin{align}
	\label{eq:bert}
	f(x_1, x_2) = -2x_1^6 &+ 12.2x_1^5 - 21.2x_1^4 + 6.4x_1^3 + 4.7x_1^2 - 
	6.2x_1 - x_2^6 + 11x_2^5 - 43.3x_2^4\\
	&+74.8x_2^3 - 56.9x_2^2 + 10x_2 + 4.1x_1x_2 + 0.1x_1^2x_2^2 - 0.4x_1x_2^2 - 
	0.4x_1^2x_2.\nonumber
\end{align}

\begin{figure}[ht!]
	\includegraphics[width=6.6cm,trim=0 20 0 30]{bertsima_2d_fun.pdf}
	\includegraphics[width=6.6cm,trim=0 20 0 30]{robust_bertsima_2d_fun.pdf}
	\includegraphics[width=6.6cm,trim=0 20 0 30]{bertsima_2d_X.pdf}
	\includegraphics[width=6.6cm,trim=0 20 0 30]{bertsima_2d_Y.pdf}
	\caption{Top: The Bertsimas function on the left and the robust surface 
		with $\alpha = 0.15$ on the right. Bottom: $d(x^*_{\mathrm{b, n}})$ 
		(left) and $r(x^*_{\mathrm{b, n}})$ (right) after 
		each acquisition.}
	\label{fig:bertsima2d}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=6.6cm,trim=0 10 0 50]{bertsima_1d_fun.pdf}
	\includegraphics[width=6.6cm,trim=0 10 0 50]{robust_bertsima_1d_fun.pdf}
	\includegraphics[width=6.6cm,trim=0 10 0 50]{bertsima_1d_X.pdf}
	\includegraphics[width=6.6cm,trim=0 10 0 50]{bertsima_1d_Y.pdf}
	\caption{Top: The Bertsimas function on the left with the robust surface 
	with $\alpha = (0.2, 0)$ on the right. Bottom: $d(x^*_{\mathrm{b, n}})$ 
	(left) and $r(x^*_{\mathrm{b, n}})$ (right) 
	after each acquisition.}
\label{fig:bertsima1d}
\end{figure}

We negated this function compared to \citeauthor{Bertsimas2010}, who were
interested in maximization. Originally, it was defined on $x_1 \in [-0.95,
3.2]$ and $x_2 \in [-0.45, 4.4]$ with $x^*=(2.8, 4.0)$.  We coded inputs to
$[0, 1]^2$ so that the true minimum is at $(0.918, 0.908)$. In the scaled
space, $x^r = (0.2673, 0.2146)$ using $\alpha = 0.15$. We used $N=90$ and
$\theta=1.1$. \blu{The supplement considers a variation where
$\hat{\theta}$ is re-estimated after each acquisition.  The results are
very similar, but noisier.} The bottom row of panels in Figure 
\ref{fig:bertsima2d} show that non-fixed $\alpha$ for REGO can give superior 
performance in early acquisitions. {\tt StableOPT} performs worse with this 
problem because the objective surface near $x^r$ is relatively more peaked, say 
compared to the 1d RKHS example.
% The true $g(x, 0.15)$ here is relatively more peaked, by recommending
% $x^*_{\mathrm{b}}$ around $x^r$ rather than at it, {\tt StableOPT} designs
% appear worse off compared to flatter robust surfaces such as the one in Figure
% \ref{fig:ryan}. 
Regret is trending toward 0 when using BEAR for acquisitions, with REGO-based
methods leading the charge. EGO with post hoc adversary performs much worse
for this problem. EGO-based acquisitions heavily cluster near $x^*$
%[further discussion in Section \ref{sec:supp}],
which thwarts consistent identification of $x^r$ even with a post hoc surrogate.

Figure \ref{fig:bertsima1d} considers the same test problem (\ref{eq:bert}),
except this time we use $\alpha = (0.2, 0)$, meaning no robustness required in
$x_2$. This moves $x^r$ to $(0.412, 0.915)$ in the scaled space. In this
problem, the robust surface is fairly flat meaning that when an algorithm
finds $x_2 = 0.915$, $x_1 \in [0.35, 0.75]$ gives a similar robust output. For
that reason, all of the methods perform worse when trying to pin down the
exact $x_1$ location, so by looking at metric $d(x^*_{\mathrm{b}})$, all 
methods appear to be doing worse, \blu{whereas}
$r(x^*_{\mathrm{b}})$  goes to 0 relatively quickly. A shallower robust
minimum favors {\tt StableOPT}.  Since that comparator never actually
evaluates at $x^r$, \blu{here} it suffices to find $x_1 \in [0.35, 0.75]$,
which it does quite easily. Knowing true $\alpha$ for REI helps considerably.
This makes sense because omitting an entire dimension from robust
consideration is informative. Nevertheless, alternatives using random and
aggregate $\alpha$-values perform well.

\section{Supplementary empirical analysis}
\label{sec:supp}

An instructive, qualitative way to evaluate each acquisition algorithm is to
inspect the final collection of samples (at $N$), to see visually if things
look better for robust variations. Figure \ref{fig:samps} shows the
final samples of one representative MC iteration for EGO, REGO with random
$\alpha$ and {\tt StableOPT} for 2d Rosenbrock
(left panel) and both Bertsimas variations (middle and right panels).

Consider Rosenbrock first.  Here, EGO has most of its acquisitions in a mass
around $x^*$ with \blu{a few} dispersed throughout the rest of the
space. This is exactly what EGO is designed to do: target the global minimum,
but still explore other areas. REGO has a similar amount of space-fillingness,
but the target cluster is focused on $x^r$ rather than $x^*$. On the other
hand, {\tt StableOPT} has almost no exploration points. Nearly all of its
acquisitions are on the perimeter of a bounding box around $x^r$. While {\tt
StableOPT} does a great job of picking out where $x^r$ is, intuitively we
do not need 70+ acquisitions all right next to each other. Some of those
acquisitions could better facilitate learning of the surface by exploring
elsewhere.

\begin{figure}[ht!]
	\includegraphics[width=4.5cm,trim=0 40 75 45,clip]{Rosenbrock_Example.pdf}
	\includegraphics[width=4.5cm,trim=0 40 75 45,clip]{Bertsima_2d_Example.pdf}
	\includegraphics[width=5.5cm,trim=0 40 0 45,clip]{Bertsima_1d_Example.pdf}
	\caption{Sample acquisitions for EGO, REGO with random $\alpha$ and 
	{\tt StableOPT} for 2d Rosenbrock with $\alpha = 0.1$ (left), and Bertsimas
	functions with $\alpha = 0.15$ (middle), and $\alpha = (0.2, 0)$ (right).}
	\label{fig:samps}
\end{figure}

Moving to the Bertsimas panels of the figure, similar behavior may be
observed. REGO and EGO have some space-filling points, but mostly target $x^r$
for REGO and $x^*$ for EGO. {\tt StableOPT} again puts almost all of its
acquisitions near $x^r$ with relatively little exploration. But the main
takeaway from the Bertsimas plots is that, since REGO does not require setting
$\alpha$ beforehand, it gives sensible designs for multiple $\alpha$ values
(the blue points are the exactly the same in both panels). 
%It would be disastrous
%to use the {\tt StableOPT} points from the design with $\alpha = 0.15$ to
%estimate $x^r$ at $\alpha = (0.2, 0)$. An explorative feature is built into
%REGO with random $\alpha$. 
Looking more closely at the REGO design, observe
that all three minima (global and robust with $\alpha = 0.15$ and $\alpha =
(0.2, 0)$) have many acquisitions around them. This shows the power of REGO,
capturing all levels of $\alpha$ and allowing the user to delay specifying
$\alpha$ until after experimental design.
\begin{figure}[ht!]
	\centering
	\vbox{\vspace{0cm}
	\includegraphics[width=4.8cm,trim=0 65 0 30,clip=true]{rkhs_Time.pdf}
	\includegraphics[width=4.8cm,trim=0 65 0 30,clip=true]{ryan_Time.pdf}
	\includegraphics[width=4.8cm,trim=0 65 0 
	30,clip=true]{bertsima_1d_Time.pdf}}
	\includegraphics[width=4.8cm,trim=0 0 0 30]{bertsima_2d_Time.pdf}
	\includegraphics[width=4.8cm,trim=0 0 0 30]{rosenbrock_2d_Time.pdf}
	\includegraphics[width=4.8cm,trim=0 0 0 30]{rosenbrock_4d_Time.pdf}
	\caption{\blu{Cumulative timings for each method/test function.}}
	\label{fig:times}
\end{figure}

\blu{Timings for each method/problem are in Figure \ref{fig:times}.}
Comparators ``egoph'' and ``ego'' report identical timings because they
involve the same EI acquisition function. As you might expect, ``unif'' and EY
have the lowest times because they do less work. For the more competitive
methods, \blu{REGO is generally a little slower than EGO, but often faster
than {\tt StableOPT}}. 
\blu{The main bottleneck in BO is the $\mathcal{O}(n^3)$ matrix
decomposition(s) required for GP likelihood evaluation and prediction.  This $n$
is changing throughout acquisitions, and there is a different final $N$ in
each expariment.  Consequently we have provided cumulative timings in Figure
\ref{fig:times}.  Since the same underlying GP implementation is shared among
all of the competitors in our study, the timings are similar (within the same
test problem), with exceptions being  ``sum'' (aggregating many GP fits),
``unif'' (not requiring a GP), and ``stable'' (a totally different approach).
When amortizing over all $N$ acquisitions, it is clear that each takes just
mere seconds even in the worst of cases.  We think this is fast enough for
most real-world black-box evaluations typically involved in BO enterprises.}

\section{REI with estimated lengthscale}
\label{app:esttheta}

\blu{Our experiments in Sections 4.2 and in Section 5 of the main text
used fixed lengthscales $\theta$ to control MC variability.   We did that so
that the focus of those experiments to be on the acquisition criteria behind
the BO algorithms, not on the surrogate modeling details -- which are shared
identically among all the methods.  However, it is worth wondering how those
results might change when lengthscales are re-estimated after each
acquisition.}
\begin{figure}[ht!]
	\centering
	\includegraphics[width=7.5cm,trim=0 20 0 50]{bertsima_2d_X_mle.pdf}
	\includegraphics[width=7.5cm,trim=0 20 0 50]{bertsima_2d_Y_mle.pdf}
	\caption{\blu{Results for 2d Bertsimas function with $\hat{\theta}$ 
	estimated via MLE: $d(x^*_{\mathrm{b, n}})$ (left) 
	and $r(x^*_{\mathrm{b, n}})$ (right).}}
	\label{fig:bertsima_mle}
\end{figure}
\blu{The results in Figure \ref{fig:bertsima_mle} show the Bertsimas function 
(from Section 4.2 of the main text)
with $\alpha=0.15$ in each dimension, akin to Figure 7 of the main text.
Each $\hat{\theta}$ is calculated by numerically maximizing the likelihood
(i.e., MLE) of the multivariate normal log likelihood for the surrogate, and the
adversarial surrogate as necessary.   The results are very similar to the
fixed-$\theta$ case, but they are noisier due to the estimation risk in
inferring these additional hyperparamers. In particular, although REI methods
perform relatively worse compared to fixed $\theta$ case in Figure
7 of the main text, they are still winning in comparison to the other
methods.}

\section{REI in higher input dimension}
\label{app:highdim}


\blu{Figure \ref{fig:rosenbrock_6d} shows the results for the 6d Rosenbrock 
function, continuing from Figure 7 of the main text in Section
4 of the main text. We use the same setup as the 4d problem, described
therein, in particular with $\alpha=0.1$ for every input coordinate. For the
initial setup of the 6d problem, we included a point within 0.02 of the
global, peaked minimum in each dimension. This is not necessary in general,
however in this high dimensional space it is very difficult to locate the
global, spiky optimum.  By nudging all solvers toward discovering this area,
we are better able showcase the merits of our robust solution, which are
designed {\em not} to be fooled by the spiky solutions.  
\begin{figure}[ht!]
	\centering
	\includegraphics[width=7.5cm,trim=0 20 0 40]{rosenbrock_6d_x.pdf}
	\includegraphics[width=7.5cm,trim=0 20 0 40]{rosenbrock_6d_y.pdf}
	\caption{\blu{The results for the 6d Rosenbrock function: 
	$d(x^*_{\mathrm{b, n}})$ (left) and $r(x^*_{\mathrm{b, n}})$ (right).}}
	\label{fig:rosenbrock_6d}
\end{figure}
Adding this additional design point represents a ``temptation'' for the
conventional, non-robust BO methods, drawing them towards the global minimum
over the robust minimum. Without it, the non-robust methods rarely find the
global minimum since the surface is so peaked and thus those methods often
report the robust minimum erroneously. We see this as short-circuiting the
outcome of a much more exhaustive search.  I.e., illustrating how each of the
methods would perform in the long run, wherein eventually the non-robust
methods would be enticed by the global minimum and that would lead to worse
acquisitions. Under this setup, and pretty much identically to the 4d example
in Section 4/Figure 7 of the main text, the REI
methods are performing the best by both metrics: they are finding the
robust minimum faster than the other methods.}

\bibliographystyle{apalike}
%\bibliographystyle{Chicago}
\spacingset{1}
\bibliography{references}

\end{document}