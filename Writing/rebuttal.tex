\documentclass[12pt]{article}
\usepackage{fullpage,epsfig}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{bm,dsfont,hyperref}
\usepackage{changes}

\graphicspath{{./Figures/}}

\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

\newcommand{\blu}[1]{\textcolor{blue}{#1}} % blue for newest version
%\newcommand{\blu}[1]{\textcolor{black}{#1}} % change edits back to black

\begin{document}
	\newcommand{\vect}[1]{\boldsymbol{#1}}
	\renewcommand{\baselinestretch}{1.5} \small\normalsize
	
	\noindent {\bf  IISE Transactions  \\ 
	\#UIIE-7609 \\ 
		``Robust Bayesian Optimization''}
	
	\bigskip
	\noindent
	Dear Professor Xinwei Deng,
	
	\medskip Thank you for allowing us to revise our manuscript.  We 
	are pleased that you think it is potentially a good fit for IISE
	Transactions.  The reviewers have made thoughtful suggestions, which we
	have addressed in our manuscript. We think that addressing these concerns
	has led to an improved manuscript and hope that you will agree.  Responses
	to the reviews' specific comments are included in the enclosed pages.
	\blu{Textual changes to the main manuscript are highlighted in blue for 
	quick reference.  In places where we cut material, there might be a single
	word remaining in blue as a place-holder.}  In several cases, especially
	with some of the new empirical work requested by the team, we have opted
	to put the results to our appendix/supplementary material.  This wasn't
	because we didn't think that the additions were important.  It was just a
	practical thing for our visuals: there were just too many lines in the
	plots, and/or too many plots.  If anyone feels strongly that they should
	be upgraded to the main body of the paper, we would happily oblige.  We
	have made sure to refer to those results/relevant appendices in the main
	body of the text.
	
	Please let us know if you need anything else from us, and thank you again 
	for your consideration of our manuscript to IISE Transactions.
	
	\vspace{0.25in}
	\begin{center}
		\begin{tabular}{l}
			Sincerely,\\
			\includegraphics[scale=0.3]{signature.png}\\
			Ryan Christianson\\
		\end{tabular}
	\end{center}
	
	\bigskip
	\renewcommand{\baselinestretch}{1.1} \small\normalsize
	
	\pagebreak
	
	\noindent
	We want to thank the referees and the associate editor for their thoughtful 
	comments.  We have attempted to address all concerns, and we think it has 
	led to a much improved manuscript.  Details are provided below.  
	\blu{Our responses are in blue, and new text in the manuscript has been 
	highlighted in blue for easy reference.}
	
	\section*{Comments from Associate Editor}
	
	Two peer reviewers and I have reviewed your manuscript. While there are 
	some innovations, I have serious concerns as follows.
	First, the way the authors introduce robust Bayesian optimization lacks 
	real-world motivations. In the fourth paragraph of Section 1, it seems that 
	the authors just treat the proposed problem as a mathematical problem, 
	without mentioning how this study could have real-world impact. 

	\medskip
	\blu{We have added text/cites to the introduction to help motivate the problem class, i.e., that robust optimization methods are important to several,
	real-world applications. We've also enhanced emphasis surrounding our own
	real-world, autonomous warehousing problem from Section 5.}

	\medskip
	\noindent 
	My specific 
	questions and comments include:
	
	\medskip
	\noindent
	1. What does "robust" mean in this context? What is this method robust 
	against?
	
	\medskip
	\blu{Thank you for the feedback. We have added more to Section 1
	to be more explicit about how we are defining robustness. We are being
	robust to the adversary that searches within a (hyper)cube of size
	$\alpha$ in each direction. This captures a sort of local worst case
	scenario within $\alpha$ given an input location.}

	\medskip
	\noindent
	2. Your problem formulation reminds me of the method of robust parameter 
	designs, where the robustness is against the uncertainty of the 
	(hard-to-control) noise factors. I wonder if a similar argument can be used 
	to justify the proposed method. This is also relevant to the choice of 
	alpha, which Reviewer 1 has a serious concern about.
	
	\medskip
	\blu{We agree this is a good connection to make, and one which we have
	clearly overlooked. In Section 1, we had mentioned how some ``robust''
	methodology deals more with noisy outputs, whereas this is more about
	noisy inputs or control variables, which is indeed more similar to our own
	setup. In particular, we agree that there are similarities to Taguchi's
	{\em robust parameter design} specifically, and have added a citation to
	that Section 1 paragraph. However, our BO approach is much different than
	design strategies from classical (locally applied) response surface
	methods.  We now note this in Section 1, but perhaps more importantly also
	at the end of Section 3.2, where the discussion centers around allowing
	disparate $\alpha_j$ in each coordinate $j$.  This is closer to the spirit
	of the Taguchi setup, however again our BO/GP nonparametric modeling
	context is much different: more global, more modern and more hands-off.}

	\medskip
	\noindent
	The quality of the presentation should be improved. As mentioned by both 
	reviewers, there are a lot of language problems. The authors should 
	carefully proofread the entire article, especially Section 1, correct the 
	errors, and polish the writing.
	
	\medskip
	\blu{Thank you for pointing this out. We have gone back through the entire 
	manuscript and made many small changes, which are not hilighted
	explicitly, to help shore up any issues with the writing.}

	\medskip
	\noindent
	I would also like to hear the authors' responses to some of the insightful 
	questions from the reviewers. For example, $y^{\alpha}(x)$ should be flat 
	in some regions. Is it still reasonable to apply Bayesian optimization for 
	this type of objective functions?
	
	\medskip
	\blu{This is a valid critique of any approach leveraging a standard,
	stationary GP fit, as we do.  The problem isn't with BO, generically, but
	with the choice of stationary surrogate model under the hood.  The more
	faithful the surrogate is to the surface it is modeling, e.g., the
	adversarial surface $y^{\alpha}(x)$, which might have flat regions, the
	better the whole enterprise will work.  Yet as you point out, we use a
	stationary GP which is a mismatch.  Perhaps it is surprising that our
	method works so well in spite of that over-simplifying choice.  On the
	other hand, one could also point out that the original test functions are
	also nonstationary: they have spiky and smooth parts.  Perhaps we are
	doing well because our acquisitions are based on a hierarchical smoothing
	of those two regimes, which is in line with the robustness goal.  In any
	case, it is worth exploring how the method might work better with a
	nonstationary surrogate, like a deep GP or a treed GP, either for the
	original surrogate, or the surrogate for the adversary, or both.  We now
	discuss this possibility in Section 6.  In fact, parts of this paragraph
	(beginning ``Despite this good empirical performance, the idea of
	robustness undermines one of the key assumptions of GP regression:
	stationarity.'') were present in the first version (that sentence was),
	but it has been upgraded in several places in response to your query ...
	so we decided to make the whole thing blue.}

	\medskip
	\noindent
	In addition, the submitted manuscript contains funding support information, 
	violating the double-blinded review rule.

	\medskip
	\blu{Our apologies for this. It has been blinded for our next submission.}
	
	\section*{Comments from Referee \#1}
	
	\bigskip
	\noindent
	The authors proposed to extend Bayesian optimization based on Gaussian 
	Process (GP) surrogates and the Expected Improvement (EI), or efficient 
	global optimization (EGO), to identify local optima that have wider domain 
	of attraction. The authors essentially sampled the GP surrogate based on 
	evaluations from EGO, fit an ``adversarial'' surrogate based on local 
	optima in the corresponding ``alpha-neighborhood'', then use EI based on 
	this adversarial surrogate to guide the sequential BO procedure. The 
	authors call this new algorithm robust expected improvement (REI) or REGO. 
	The authors tested REI and compared with other BO algorithms on both simple 
	functions and a robot pushing application.
	
	\medskip
	\noindent There are several major concerns:
	
	\medskip
	\noindent 
	1. The proposed REI procedure does not seem to have any theoretical 
	analysis and it is not clear what performance guarantee that REI may have.

	\medskip
	\blu{This is true and a fair criticism.  I guess we're just not that kind
	of statistician.  For what it's worth, this is the only point in this
	rebuttal/revision that we are punting, a bit.  Because we're not going to
	become theoretical math-stat folks in a few months, we don't have any
	theorems to add to the document.  However, we have added a paragraph about
	theory to our revised discussion in Section 6.  There is some theory for
	EI, about optimal greedy decision-making, and about convergence.  Since we
	are basically doing EI, but on a different (adversarial) surrogate, we
	think that at least some of that theory applies, in the sense that what we
	are doing is reasonable; not in the sense of any absolute guarantees.  EI
	doesn't even have that, since the regularity conditions are unrealistic.
	The math programming literature has plenty of theory (in the papers we now
	cite in our revision) providing convergence guarantees, etc.  In spite of
	that, we have found the results reported in those papers to be
	un-competitive.  In practice it takes many hundreds, even thousands of
	acquisitions to reach a ``convergence'' threshold.  Our experiments, in
	line with most BO applications, involve smaller, fixed budgets in the
	dozens of runs. We have commented on this in our revised Section 6 as
	well. In summary, we hope that our empirical evidence, which is very
	strong, and our open source implementation will suffice.  We think that
	the theory, should someone smarter than us take a crack at it, will
	support that empirical evidence.  But, of course, that's highly
	speculative, which is why such comments are relegated to the discussion at
	the end.}
	
	\medskip
	\noindent
	2. The fitting of ``adversarial'' surrogate should be explained clearly. 
	Based on the current description, it is not clear how it was actually 
	implemented since the first GP surrogate is non-parametric, its 
	corresponding expectation is based on the sequential evaluations. The 
	authors may need to be clearer on how the ``adversarial'' simulations 
	should be done for fitting this second surrogate and explain how the actual 
	implementation may affect the performance.
	
	\medskip
	\blu{Many thanks.  We have tried to more clearly explain our methodology
	(Section 3.1) and implementation (Section 4) to help here.   This is a
	very important point, as it relates to the cornerstone of our method:
	adversarial data and the surrogate we fit to it.  In particular, see blue
	text around Eq.~(6) in Section 3.1 where we explain that the adversarial
	data $y_i^\alpha$ come from the mean of the surrogate fit to the original
	data.  There are no simulations here, or anywhere in the our analysis.
	Everything, except the initial designs in our empirical work (coming from
	LHS), is deterministic. In Section 4.1, new text (in blue) explains that
	all surrogates, but in particular the adversarial surrogate, uses maximum
	likelihood estimation to get $\hat{\tau}^2$ based off of lengthscale
	$\theta$. This $\theta$ value is fixed for each problem, for both
	surrogates as described in Section 4. However, new empirical work in
	Appendix A provides similar results for estimated lengthcales.   New text
	was also added in Section 4.1 to detail our preferred method for
	numerically solving for the adversarial data, $y_i^\alpha$, implementing
	Eq.~(6).  We hope that these changes, coupled with the description in
	Algorithm 1 (which is unchanged), provides enough detail on the overall
	flow, and the specifics of our implementation.}

	\medskip
	\noindent
	3. Section 3.2 page 13 on the choice of alpha. It appears that the choice 
	of alpha was converted to a problem of either sampling or marginalizing 
	from 0 to another hyperparameter $\alpha_{max}$. First, the authors may 
	need to be clear on the preferred ``robustness'', which appear to be 
	directly related to $\alpha$ based on their motivation. Clearly, with 
	increasing $\alpha$, $x^r$ increases in (2). If there is no clear 
	preference, I would think it is difficult to evaluate whether the derived 
	results are good or bad. Second, it is still not clear how $\alpha_{max}$ 
	should be decided.
	
	\medskip
	\blu{This is a great point: $\alpha_{\max}$ introduces a new 
	hyperparameter. We have added some text in Section 3.2 in order to be
	transparent more about this. However, it's not an {\em additional}
	hyperparameter, since it replaces $\alpha$.  So there are the same number
	of knobs to dial in, as it were.  Our purpose in this is twofold.  First,
	we think it might be easier for a practitioner to commit to an upper-limit
	on $\alpha$, rather than a specific value.  Second, this allows us to
	illustrate, in our empirical work, that it's not necessary to dial in
	exactly the right $\alpha$-value in order to get results (good BEAR
	values) against that true (but unknown) $\alpha$ with some $\alpha_{\max}
	> \alpha$ and a degree of marginalization, as you say.  We have bolstered
	our introduction (revised Section 1) to be more clear about what we mean
	by ``robustness'' in a primary, formal, sense.  We added additional text
	to the end of Section 3 on being additionally robust to choices of
	$\alpha$. This second type of robustness is what we refer to as ``doubly
	robust'' -- to $\alpha$ with $\alpha_{\max}$ -- in our discussion in
	Section 6.}

	\medskip
	\noindent
	4. There does not seem to be any conclusive discussions when comparing REI 
	with other BO algorithms. The authors illustrated the different trends but 
	may need to provide a more comprehensive and convincing evaluation 
	experimental design to demonstrate the benefits of extend EGO to REI/REGO.
	
	\medskip
	\blu{We think the ``trends'' you are referring to comprise of very strong
	evidence about REI's superiority over conventional BO algorithms. However,
	perhaps we did not make this observation forcefully enough in our
	narration.  For example, in Section 4.2 we had a statement that read ...
	``Those based on BOV like `ego' and `ey', fare worst of all''.  We now
	clarify as ... `Those based on BOV like `ego' and `ey', \uline{i.e., the
	conventional BO methods}, fare worst of all'', in order to clarify that we
	are winning against ``other BO algorithms''.  Here we have also added that
	``It is worth noting that all other methods, i.e., besides ``ego'', ``ey''
	and ``unif'', utilize adversarial reasoning.  All but {\tt stableOPT} in
	that group deploy some number of elements comprising our novel
	methodological contribution, i.e., a post hoc adversary, or full REGO.''
	The story is similar for our other synthetic examples in Section 4, but we
	have tried to be more subtle, so as not to be repetitive, in our
	narrative.  However, we have additionally provided new text at the end of
	Section 5 to highlight that REI is in fact performing better than the
	other BO methods on this real data, physics-based robot pushing example.
	Again here (Figure 12 and 14), it is clear that ordinary BO methodology is
	not sufficient to find the robust minimum.  However, we acknowledge that
	it's a closer call between methods which respect the ``robust'' nature of
	the desired solution, and we have tried to convey this accurately in our
	revised narrative.}

	\medskip
	\noindent
	5. The presented results are quite preliminary with the highest input space 
	dimension at 4. The authors may need to provide more realistic example to 
	demonstrate whether REI is practically useful. As it also involves 
	different heuristics to fit adversarial surrogate and then compute EI, 
	clear computational complexity should be also provided.
	
	\medskip
	\noindent
	\blu{This is a fair point, and we spent a fair bit of time addressing this
	in our revision.  The real challenge here isn't applying our method to
	bigger problems.  It is in finding bigger problems that benefit from our
	robust approach.  I.e., where there is a deceptive, sharp local minima and
	a wider more robust alternative.  Ultimately we decided on a 6d version of
	the Rosenbrock problem.  We include the results of this experiment with
	our supplementary material at the end of the paper (Appendix B) because
	the results were very similar to the 2d and 4d versions in the main body
	of the paper (Section 4.2).  We see no reason why results would not be
	similar in other, modestly dimensioned setting.}

	\blu{BO methods are not known to work well in very high dimension ($\geq$
	10 or so) because GP surrogate models become data hungry in those
	settings, whereas the whole goal of BO is to keep the data set small.
	Most real-world settings where BO is applied are like our robot pushing
	problem.  Our goal isn't to advance the frontier of BO in terms of input
	dimension, although folks are certainly working in that area.  We have
	added a discussion centered around merging our adversarial ideas with this
	frontier in our Section 6 discussion.}

	\blu{As for timings, we note that these were provided in Figure 11 but the
	discussion here was possibly too terse.  We have now augmented that in our
	revision, in particular to discuss where the main computational costs are
	coming from -- $O(N^3)$ matrix decomposition needed to fit underlying GP
	models -- and to explain that when amortized over all acquisitions, the
	result is mere seconds per decision.  We think that timings like these
	make such BO scheme(s) fast enough for most real-world simulators, $f$,
	involving computationally intensive blackbox evaluations.}

	\medskip
	\noindent 
	6. There are typos, grammar errors, incomplete sentences, and other 
	language problems. For example, the last time of page 1 and 2nd line of 
	page 2: "a criteria"; line 44 of page 2: "... may naturally express a 
	preference ones which enjoy a wider domain of attraction...", and many 
	other sentences that are confusing throughout the submission. The authors 
	may need to improve their presentation significantly. 
	
	\medskip
	\blu{Thank you for pointing these out. We have gone through and corrected 
	typos and fixed sentences throughout, focusing on ones pointed out by the
	editorial team in particular, but also identifying and fixing some novel
	ones as well.}

	\section*{Comments from Referee \#2}
	
	This paper proposes a new surrogate modeling and active learning technique 
	for robust optimization within the Bayesian optimization framework, where a 
	local minima with a larger troughs is preferred. Specifically, the authors 
	propose a robust version of the expected improvement (EI) named as robust 
	expected improvement (REI). Instead of the original EI, the REI builds the 
	surrogate model based on the perturbed data points (adversarial samples),
	and the rest of REI is similar to that of EI. Numerical experiments are 
	conducted to show the efficiency of the proposed algorithm. Overall, the 
	paper is well-written and the method is clearly presented. My specific 
	comments are as follows.

	\medskip
	\noindent
	1. My first comment is about the adversarial surrogate. I am wondering why 
	it is reasonable to impose a Gaussian prior on the adversarial samples, 
	because I do not think the adversarial samples follow a Gaussian 
	distribution. For example, in Figure 1, g(x, 0.05) has many flat regions, 
	which are clearly non-Gaussian.
	
	\medskip
	\blu{This is an important question that gets to the heart of GP surrogate 
	modeling.  It is important to appreciate -- and we have made upgrades to 
	our review in Section 2 in our revised manuscript to help -- that the
	Gaussian distribution is not being placed on the response $y$, or even the
	adversarial response $y^\alpha$, but on the latent random field underlying
	the original function $f(\cdot)$ and its adversary $g(\cdot, \alpha)$. In
	this paper we treat the responses, and their adversarial mapping, as
	deterministic.  They have no distribution at all!  The thing that's random
	is the ``space between'' the observations, which is presumed to be MVN
	with a covariance structure governed by inverse distance.  Here, the
	Gaussian distribution is a matter of convenience, because Gaussians lend a
	high degree of analytical tractability.  There are other, more cumbersome
	choices in the BO literature.  But we are focusing on the conventional
	GP-based BO approach.  Like in those works, our novel contributions
	compare favorably, both on absolute and relative terms, despite potential
	mismatch in the underlying dynamics behind the response surfaces in
	question.  In addition to the new commentary in Section 2, we have
	added discussion to our revised Section 6 on how additional flexibility
	could be advantageous in certain contexts, for example to relax the
	stationarity assumptions behind the distance-based covariance structure,
	and to handle noisy responses.  The noise can follow any distribution, but
	we still call it GP modeling if the latent field is MVN.}

	\medskip
	\noindent
	2. When introducing the methodology, the authors often use ``estimated 
	hyperparameters'' $\theta_n$. First of all, I doubt that it is reasonable 
	to 
	call them ``hyperparameters''. Second, I did not find how to estimate these 
	parameters. Instead, the authors impose a value of $\theta_n$ in their 
	numerical experiments, and mentioned that this is ``to control MC 
	variation''. However, I believe that the value of $\theta_n$ could 
	significantly change the learning process, and thus the performance of the 
	algorithm. Could the authors provide some guidelines for choosing/learning 
	an appropriate $\theta_n$ in practice and show some numerical experiments 
	about the performance when choosing different $\theta_n$?

	\medskip	
	\blu{Thanks for this comment.  We'd like to start by saying that we didn't
	choose to call them ``hyperparameters''.  This is the convention in the
	surrogate modeling literature, and in textbooks (e.g., Santner, Williams,
	and Notz, 2018).  The reason for this is that they don't have a big effect
	on the resulting predictions, and on other downstream tasks, as long as
	they are not dialed into pathological settings.  But you don't have to
	take our word for it.  It is totally legitimate to ask how reasonable
	settings could be determined, and/or how they could be inferred from data
	and how that affect results, e.g., in a BO context.}

	\blu{Consequently, we have added more detail to the narrative, and added 
	new experimental results to help better appreciate the relationship
	between these hyperparamers and our robust BO results.  In particular, new
	text now explains how we estimate $\hat{\tau}^2$ via MLE in Section 4.1.
	We were always estimating this hyperparameter in our empirical work,
	refreshed after each acquisition, even in the originally submitted
	manuscript.  We have also now added discussion and empirical work to our
	supplementary material (Appendix A), describing how $\hat{\theta}$ may be
	estimated, also via MLE.   Using that scheme, we re-ran one of our earlier
	MC bake-offs to show how the results change.  We observe that the only
	difference is that the relative comparison is ``noisier'', and we explain
	that this is because estimating an additional quantity (a hyperparameter)
	introduces additional estimation risk.  In our revision we explain that,
	in general, estimating unknown quantities is the right way to go.
	However, hyperparaters like $\theta$ are involved in all of the methods we
	compare, because they all use GPs. But we are not evaluating those GPs.
	Rather, we are evaluating the BO acquisition schemes which are being
	deployed downstream of those surrogates. Therefore, to get a clearer
	picture of how those BO schemes compare, we decided to fix their GP
	hyperparameterization in order to better control the variability of our
	performance metrics, as evaluated over thousands of MC repeats.}

	\medskip
	\noindent
	3. I am kind of confused when reading the discussion after Figure 1 in 
	Introduction. When I read the discussion, I do not know what is g(x, 0.05) 
	until I turn to the next page, and there is no indication in the discussion 
	about where can I find the definition of function g. I do not know what are 
	“Alpha Window” and the dots representing in Figure 1. Although I can 
	roughly get the idea of the discussion, I think these need to be clarified.
	
	\medskip
	\blu{Thank you for helping us introduce our problem better by letting us 
	know what is confusing about Figure 1. We have gone through this figure
	and added text to ensure all information regarding Figure 1 is presented
	on page 2 when single spaced. Unfortunately, the double spaced version
	spills some of the explanation onto page 3.  Things will be different yet
	again once the paper is re-formatted into the journal style.  Perhaps this
	is the nature of floating figures in \LaTeX.  However, we will work to
	make sure things read well, once in its final form.  Additionally, we
	have changed ``$\alpha$ window'' to ``$\alpha$-neighborhood'' since that
	is how we introduce it in the main text.  We apologize for that oversight,
	which might have been contributing to some of the confusion.}

	\medskip
	\noindent
	4. I am curious if other acquisition functions also work, since it seems 
	that only the data used in the acquisition function have been changed.
	
	\medskip
	\blu{This is a good question. In our empirical work, we showed that the
	post hoc surrogate can be applied to get reasonable results without a
	robust acquisition function, but things are even better when the
	acquisition function is robust (i.e., our flagship REGO). We have
	augmented our discussion in Section 6 in our revision to mention other
	acquisitions, such as based on upper-confidence bounds and Thompson
	sampling (with citations), which could be applied to the adversary as
	well. We chose EI because that is most familiar to us, but others would
	likely work as well.}

	\medskip
	\noindent
	5. I think there is a lack of a thorough literature review. For example, 
	robust optimization itself is an important problem in operations research. 
	I think some literature in operations research are missing and should be 
	introduced.
	
	\medskip
	\blu{Indeed, the world of robust optimization, particularly in the
	mathematical programming literature, is wider than we had acknowledged. To
	help bridge this gap, we now provide a more thorough literature review in
	our Section 1 introduction, focusing our revision on methods from non-BO
	communities. We have also commented, in our revision, on the subset of
	non-BO robust optimizers that have entertained the Bersimas example from
	Eq.~(12) in Section 4.1, which is related to your next comment.}

	\medskip
	\noindent
	6. Following the previous comment, I think some state-of-the-art methods 
	from operations research should be included in the numerical experiments. 
	Also, for the numerical experiments part, I am curious why $d = 4$ is high 
	dimension. I think it could be better to include an example for 10 
	dimensional case.

	\medskip
	\blu{There are two important points here, on comparators and problem size
	which we shall respond to in turn.}

	\blu{Non-BO comparators: In our revised Section 4.1, we now briefly
	outline the empirical performance of other popular robust optimizers on
	the Bertsimas example function (Section 4.2). This problem is a common
	benchmark example for robust optimization, so it has been done by several
	other authors, whose papers we now cite.  However, we also point out the
	spirit of our contribution is different from those previous publications,
	and to the math programming approach to optimization in general.  We, and
	the BO literature at large, are primarily interested in
	expesive-to-evaluate blackbox functions $f$. This expense means that we
	can only make a small handful (say dozens) of evaluations.  We can't
	afford to iterate to ``convergence'', which is common in math programming.
	So our exercises focus around exploring performance under strict
	evaluation budgets.  E.g., for the Bertsimas function (Eq.~12), we go up to 
	$N=90$, but we see
	good results as early as $n=40$ for our best methods, like REGO. By
	contrast, the non-BO methods that we have surveyed attempt to find the
	true robust minimum within a small threshold, which in turn means numbers
	of evaluations, $n$, into the many hundreds or thousands.  This is not
	competitive, or worth putting in our figures because the lines would go
	off the page.  But to be fair, the goal is not the same.  It is also
	important to point out that we provide a global optimizer, whereas many of
	the popular non-BO optimizers offer only local convergence.  We have worked
	to make this argument and qualitative comparison in our revised narrative,
	with new blue text in Section 4.1.}

	\blu{High dimension: We do not wish to claim that $d=4$ is ``high'', and
	if we have given that impression we apologize.  At one point we said
	``higher dimension'', meaning higher than our previous, one-dimensional
	examples.  Still, we agree that $d = 4$ may not be convincing enough, and
	this was a point made also by the other referee. Consequently we have
	added a 6d example to our Appendix. In general, BO methods excel in
	``modest'' input dimension, which we now discuss in Section 6.  This
	generally means less than ten. The trouble with even higher dimension is
	that you need lots more training data to fit GPs in that huge space.  But
	this is the opposite of the BO goal of limiting expensive evaluations.
	Consequently, new surrogate modeling ideas are needed, which we also now
	discuss in our revised Section 6.}

	\blu{One reason we did not do a 10d example, as you suggested, is that we
	couldn't find a good one.  Not just any example will do.  We need one that
	has a deceptive, sharp global minimum, and more attractive wider local one
	to target as a robust solution.  We could not find one, and had trouble
	manifesting one, in 10d.  We did, however, extend our Rosenbrock example to
	6d.  It could also have been extended to 10d, but preliminary experiments
	suggested that training data sizes upwards of $N=1000$ would have been
	required for {\em any} of the BO methods to find the local domains of
	attraction.  We decided that the computational costs in our MC setup
	(involving cubic in $N$ runtimes) were too high, and had no reason to
	believe that it would pay off, i.e., that the results would be any
	different or more interesting than our new 6d ones.}

	\blu{Ultimately, we are happy to give the impression that our method works
	in modest (or even modestly low) input dimension, beating conventional BO
	methods in the robust context.  All GP methods face the same surrogate 
	modeling challenges as dimension increases.  High input dimension is on the 
	BO frontier, and it will be exciting to entertain what this means for robust
	optimization in future work.}

	\medskip
	\noindent
	7. There are some typos in the manuscript. For example, Page 5, line 2, “an 
	surrogate”. Page 16, line 18 “the4”.
	
	\medskip
	\blu{Thank you for pointing those out to us. We have corrected them,
	along with some other mistakes we discovered in our own subsequent reads.}

\end{document}